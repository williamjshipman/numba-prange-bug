import numba
import numpy as np

def MakeKernel(Method):

    @numba.jit(nopython=True)
    def Kernel(X):
        if Method == 1:
            length = X.size
        else:
            length = X.size * 3
        Y = np.arange(length)
        Accumulator = 0.0
        for idx in range(length):
            Accumulator += Y[idx] * X[idx % X.size]
        return Accumulator

    return Kernel

def MakeParallel(KernelGenerator, Param):
    Kernel = KernelGenerator(Param)

    @numba.jit(nopython=True, parallel=True)
    def ParallelKernel(Data, Output):
        for idx in numba.prange(Data.shape[0]):
            Output[idx] = Kernel(Data[idx, :])
        return Output

    return ParallelKernel

def MakeSerial(KernelGenerator, Param):
    Kernel = KernelGenerator(Param)

    @numba.jit(nopython=True, parallel=True)
    def SerialKernel(Data, Output):
        for idx in numba.prange(Data.shape[0]):
            Output[idx] = Kernel(Data[idx, :])
        return Output

    return SerialKernel

Kernel = MakeParallel(MakeKernel, 0)
# Kernel = MakeSerial(MakeKernel, 0)

Data = np.arange(100, dtype=np.float64).reshape(5,20)
Output = np.zeros((Data.shape[0],), dtype=np.float64)

print(Kernel(Data, Output))